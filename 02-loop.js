'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });
    
    main();    
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the vowelsAndConsonants function.
 * Print your output using 'console.log()'.
 */
function vowelsAndConsonants(s) {
    let vokal = [];
    let kons = [];
    for(var i = 0; i < s.length; i++ ){
        if(
            s[i] == 'a' ||
            s[i] == 'i' ||
            s[i] == 'u' ||
            s[i] == 'e' ||
            s[i] == 'o' 
            ){
            vokal.push(s[i]);
        }else{
            kons.push(s[i]);
        }
    }
    for(var i = 0; i < vokal.length; i++ ){
        console.log(vokal[i]);
    }
    for(var i = 0; i < kons.length; i++ ){
        console.log(kons[i]);
    }
}


function main() {
    // const s = readLine();
    const s = 'javascriptloops';
    
    vowelsAndConsonants(s);
}

main();